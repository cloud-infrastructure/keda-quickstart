KEDA: A Quickstart Guide
===

## Installation

1. Add Helm repo
```
helm repo add kedacore https://kedacore.github.io/charts
```
2. Update Helm repo
```
helm repo update
```

3. Install keda Helm chart
```
kubectl create namespace keda
```

```
helm install keda kedacore/keda --namespace keda
```

### Uninstall

If you want to remove KEDA from a cluster you can run one of the following:
```
helm uninstall keda -n keda
```

You could find more informations/options regarding keda installation [here](https://keda.sh/docs/2.4/deploy/).

## KEDA: Publishing and Consuming Events with RabbitMQ Scaler 

In the following [guide](https://github.com/kedacore/sample-go-rabbitmq) you will consume and publish messages in a RabbitMQ queue using KEDA for scaling.

### Pre-requisites

* Kubernetes cluster.
* KEDA 2.4 installed on the cluster.

### Before start following the RabbitMQ sample
In order to install a RabbitMQ instance in your cluster following the aformentioned guide you need to create and apply the following yaml file:
```
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: data-rabbitmq-0
spec:
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 2G
  storageClassName: geneva-cephfs-testing
```